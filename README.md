# Adzooma Frontend - Testing

This project roughly outlines the testing approach to Frontend, within the realms of JavaScript / React.

The techologies suggested are:

- Jest
- React Testing Library
- Cypress

## Overview

The following is covered within this example project.

## Testing

Differnet levels of testing include.


| Test Type   | Tools Used                  | Examples         | Status              |
|-------------|-----------------------------|------------------|---------------------|
| Unit        | jest, react-testing-library | src/helpers*     | IN PROGRESS         |
| Component   | jest                        | src/components/* | IN PROGRESS         |
| Integration | jest, nock                  | src/service/*    | IN PROGRESS         |
| Functional  | cypress                     | N/A              | AWAITING DISCUSSION |

### Unit Testing

See `src/helpers/add.js`. This outlines the basics or unit testing. A helper function has been created which adds two numbers togther. The unit tests sit alongside source in this example.

### Integration Testing

See `src/service/catFacts.js`. This outlines how an integration test would look when interacting with another system (i.e. an API). The tools used here outline how to mock an endpoint to assert failure within logic that service houses.

### Component Testing

See `src/components/Hello.jsx`. A basic overview of snapshot testing. Todo: outline how react testing library should be used and what features are useful.

## Pipelines

The suggested pipeline is as follows.

| Stage   | Commands                 | Description                                                                                | Halts Build? |
|---------|--------------------------|--------------------------------------------------------------------------------------------|--------------|
| clone   | git clone <repo>         | Clones the source code from Bitbucket                                                      | Yes          |
| install | npm ci                   | Installs the dependencies required to run the build                                        | Yes          |
| build   | npm run build            | Builds the project via webpack (e.g. example)                                              | Yes          |
| test    | npm run test             | Runs the entire test suite                                                                 | Yes          |
| test    | npm run test:unit        | Runs only the unit tests                                                                   | Yes          |
| test    | npm run test:integration | Runs only the integration tests                                                            | Yes          |
| test    | npm run test:functional  | Runs only the functional tests                                                             | Yes          |
| lint    | npm run lint             | Lints the project. Can be configured to warn rather than halt.                             | No*          |
| audit   | npm run audit            | Checks dependencies for severe / high security warnings. Medium and Low could be ignored.  | No*          |

