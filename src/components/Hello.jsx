import React from 'react';
import PropTypes from 'prop-types';

const Hello = ({ name, greeting }) => (
  <h1>
    {greeting}

    {name}
  </h1>
);

Hello.defaultProps = {
  greeting: 'Hi',
};

Hello.propTypes = {
  name: PropTypes.string.isRequired,
  greeting: PropTypes.string,
};

export default Hello;
