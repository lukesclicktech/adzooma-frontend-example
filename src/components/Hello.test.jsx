import React from 'react';
import renderer from 'react-test-renderer';
import Hello from './Hello';

describe('components/Hello', () => {
  test('should return a function', () => {
    expect(typeof Hello).toBe('function');
  });

  test('should render correctly', () => {
    const name = 'Luke';
    const component = renderer.create(<Hello name={name} />).toJSON();

    expect(component).toMatchSnapshot();
  });

  // test('should change the greeting when the prop is passed', () => {
  //   const component = renderer
  //     .create(<Hello name='Luke' greeting='Howdy' />)
  //     .toJSON();
  // });
});
