import nock from 'nock';
import getCatFactText from './catFacts';

describe('cat-facts', () => {
  describe('integration', () => {
    test('should return a cat fact', async () => {
      const text = await getCatFactText('58e0088b0aac31001185ed09');

      expect(text).toBe("The world's largest cat measured 48.5 inches long.");
    });

    test('should throw an error if the cat-fact service fails', async () => {
      const id = '58e0088b0aac31001185ed09';

      nock('https://cat-fact.herokuapp.com').get(`/facts/${id}`).reply(500);

      await expect(getCatFactText(id)).rejects.toEqual(
        new Error(`Unable to fetch cat-fact: ${id}`),
      );
    });
  });
});
