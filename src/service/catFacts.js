import fetch from 'cross-fetch';

const getCatFactText = async (id) => {
  const response = await fetch(`https://cat-fact.herokuapp.com/facts/${id}`);

  if (response.ok) {
    const obj = await response.json();

    return Promise.resolve(obj.text);
  }

  return Promise.reject(new Error(`Unable to fetch cat-fact: ${id}`));
};

export default getCatFactText;
