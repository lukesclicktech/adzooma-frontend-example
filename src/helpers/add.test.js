import add from './add';

describe('helpers/add', () => {
  test('should return 2 when add(1, 1) are passed', () => {
    expect(add(1, 1)).toBe(2);
  });

  test('should return 2 when add(1, 1) are passed', () => {
    expect(add(1, 1)).not.toBe(3);
  });
});
